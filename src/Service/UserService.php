<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\CompanyRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserService
{
    private EntityManagerInterface $entityManager;
    private UserPasswordEncoderInterface $passwordEncoder;
    private UserRepository $userRepository;
    private CompanyRepository $companyRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        UserRepository $userRepository,
        CompanyRepository $companyRepository
    ) {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->userRepository = $userRepository;
        $this->companyRepository = $companyRepository;
    }

    public function add(User $user): UserInterface
    {
        $encodedPassword = $this
            ->passwordEncoder
            ->encodePassword(
                $user,
                $user->getPassword()
            );

        $this
            ->entityManager
            ->persist(
                $user
                    ->setPassword($encodedPassword)
                    ->setStatus('UNCONFIGURED')
                    ->setRoles(['ROLE_USER'])
            );

        $this->entityManager->flush();

        return $user;
    }

    public function updateStatus(
        User $user,
        string $string
    ): void {
        $user->setStatus($string);
        $this->entityManager->flush();
    }
}
