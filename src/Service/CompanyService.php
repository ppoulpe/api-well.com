<?php

namespace App\Service;

use App\Entity\Company;
use App\Entity\User;
use App\Repository\CompanyRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class CompanyService
{
    private EntityManagerInterface $entityManager;
    private UserRepository $userRepository;
    private CompanyRepository $companyRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        CompanyRepository $companyRepository
    ) {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->companyRepository = $companyRepository;
    }

    public function add(Company $company): Company
    {
        $company->setCreatedAt(new DateTime('now'));
        $this->entityManager->persist($company);
        $this->entityManager->flush();

        return $company;
    }

    public function associate(
        Company $company,
        User $user
    ): void {
        $user->setCompany($company);
        $this->entityManager->flush();
    }
}
