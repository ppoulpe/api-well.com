<?php

namespace App\Entity\ValueObject;

use MyCLabs\Enum\Enum;

class UserStatus extends Enum
{
    private const UNCONFIGURED = 'UNCONFIGURED';
    private const ACTIVE = 'ACTIVE';
}
