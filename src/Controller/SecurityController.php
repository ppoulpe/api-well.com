<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\UserService;
use JsonSerializable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class SecurityController extends AbstractController
{
    private UserService $userService;

    private SerializerInterface $serializer;

    public function __construct(UserService $userService, SerializerInterface $serializer)
    {
        $this->userService = $userService;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/auth/login", name="app_login", methods={"OPTIONS","POST"})
     */
    public function login(Request $request): JsonResponse
    {
        return $this->json([
            'Hell yeah',
        ]);
    }

    /**
     * @Route("/auth/register", name="app_register", methods={"PUT"})
     */
    public function register(Request $request): JsonResponse
    {
        return new JsonResponse(
            $this->userService->add(
                $this->serializer->deserialize(
                    $request->getContent(),
                    User::class,
                    'json'
                )
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/auth/me", name="app_me", methods={"OPTIONS", "GET"})
     */
    public function me(Request $request): JsonResponse
    {
        /**
         * @var JsonSerializable|User $user
         */
        $user = $this->getUser();

        return $this->json($user->jsonSerialize());
    }
}
