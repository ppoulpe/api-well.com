<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\User;
use App\Entity\ValueObject\UserStatus;
use App\Service\CompanyService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CompanyController extends AbstractController
{
    private CompanyService $companyService;
    private SerializerInterface $serializer;
    private UserService $userService;

    public function __construct(
        CompanyService $companyService,
        UserService $userService,
        SerializerInterface $serializer
    ) {
        $this->companyService = $companyService;
        $this->serializer = $serializer;
        $this->userService = $userService;
    }

    /**
     * @Route("/company", name="app_create_company", methods={"OPTIONS", "PUT"})
     */
    public function create(Request $request): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $company = $this->serializer->deserialize(
            $request->getContent(),
            Company::class,
            'json'
        );

        $this->companyService->add($company);
        $this->companyService->associate($company, $user);
        $this->userService->updateStatus($user, UserStatus::ACTIVE());

        return new JsonResponse(
            $company,
            Response::HTTP_OK
        );
    }
}
