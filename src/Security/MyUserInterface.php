<?php

namespace App\Security;

use App\Entity\Company;
use Symfony\Component\Security\Core\User\UserInterface;

interface MyUserInterface extends UserInterface
{
    public function setCompany(Company $company): self;
}
