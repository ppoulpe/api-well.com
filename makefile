phpstan:
	@vendor/bin/phpstan analyse src

cs-fixer:
	@php vendor/bin/php-cs-fixer fix

grumphp:
	vendor/bin/grumphp run