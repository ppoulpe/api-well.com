<?php

namespace App\Tests\api;

use App\Tests\ApiTester;

class CreateUserCest
{
    // tests
    public function tryToTest(ApiTester $I)
    {
        //$I->amHttpAuthenticated('service_user', '123456');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('auth/register', [
            'email' => 'jerome.dumas@well.com',
            'password' => 'awesomePasswordPhrase',
            'name' => 'name',
            'surname' => 'surname',
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'email' => 'jerome.dumas@well.com',
            'status' => 'UNCONFIGURED',
            'name' => 'name',
            'surname' => 'surname',
        ]);
    }
}
