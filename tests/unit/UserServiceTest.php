<?php

namespace App\Tests\unit;

use App\Entity\User;
use App\Entity\ValueObject\UserStatus;
use App\Repository\CompanyRepository;
use App\Repository\UserRepository;
use App\Service\UserService;
use App\Tests\UnitTester;
use Codeception\Test\Unit;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserServiceTest extends Unit
{
    protected UnitTester $tester;
    protected EntityManagerInterface $em;
    protected UserService $service;

    /**
     * @var UserPasswordEncoderInterface | MockObject
     */
    protected UserPasswordEncoderInterface $passwordEncoder;

    protected function _before()
    {
        $this->em = $this->getModule('Doctrine2')->em;
        $this->passwordEncoder = $this->makeEmpty(UserPasswordEncoderInterface::class);
        $this->service = new UserService(
            $this->em,
            $this->passwordEncoder,
            $this->make(UserRepository::class),
            $this->make(CompanyRepository::class)
        );
    }

    public function testAddUser()
    {
        $this
            ->passwordEncoder
            ->expects($this->once())
            ->method('encodePassword')
            ->willReturn('encodedPassword');

        $this->service->add(
            (new User())
                ->setEmail('jerome.dumas@well.com')
                ->setPassword('password')
                ->setName('Dumas')
                ->setSurname('Jérôme')
        );

        $user = $this
            ->tester
            ->grabEntityFromRepository(
                User::class,
                ['email' => 'jerome.dumas@well.com']
            );

        $this->assertEquals(
            [
                'encodedPassword',
                ['ROLE_USER'],
                UserStatus::UNCONFIGURED()->getValue(),
            ],
            [
                $user->getPassword(),
                $user->getRoles(),
                $user->getStatus(),
            ]
        );
    }

    public function testUpdateUserStatus(): void
    {
        $userId = $this
            ->tester
            ->haveInRepository(
                User::class,
                [
                    'id' => 1234,
                    'email' => 'jerome.dumas@well.com',
                    'name' => 'Dumas',
                    'surname' => 'Jérôme',
                    'password' => 'encodedPassword',
                    'roles' => ['ROLE_USER'],
                    'status' => UserStatus::UNCONFIGURED()->getValue(),
                    'created_at' => new DateTime('now'),
                    'company' => null,
                ]
            );

        $this->service->updateStatus(
            $this->em->find(User::class, $userId),
            UserStatus::ACTIVE()->getValue()
        );

        $user = $this
            ->tester
            ->grabEntityFromRepository(
                User::class,
                ['email' => 'jerome.dumas@well.com']
            );

        $this->assertEquals(
            UserStatus::ACTIVE()->getValue(),
            $user->getStatus()
        );
    }
}
