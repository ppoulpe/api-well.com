<?php

namespace App\Tests\unit;

use App\Entity\Company;
use App\Entity\User;
use App\Repository\CompanyRepository;
use App\Repository\UserRepository;
use App\Service\CompanyService;
use App\Tests\UnitTester;
use Codeception\Exception\ModuleException;
use Codeception\Test\Unit;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class CompanyServiceTest extends Unit
{
    protected UnitTester $tester;
    private CompanyService $service;
    private EntityManagerInterface $em;

    protected function _before()
    {
        $this->em = $this->getModule('Doctrine2')->em;
        $this->service = new CompanyService(
            $this->em,
            $this->make(UserRepository::class),
            $this->make(CompanyRepository::class)
        );
    }

    // tests
    public function testAddCompany(): void
    {
        $this->service->add(
            (new Company())
                ->setName('My awesome company')
        );

        $this
            ->tester
            ->seeInRepository(
                Company::class,
                ['name' => 'My awesome company']
            );
    }

    /**
     * @test
     *
     * @throws ModuleException
     */
    public function testAssociateCompany(): void
    {
        $userId = $this
            ->tester
            ->haveInRepository(
                User::class,
                [
                    'id' => 1234,
                    'email' => 'jerome.dumas@well.com',
                    'name' => 'Dumas',
                    'surname' => 'Jérôme',
                    'password' => 'password',
                    'status' => 'UNCONFIGURED',
                    'created_at' => new DateTime('now'),
                    'company' => null,
                ]
            );

        $companyId = $this
            ->tester
            ->haveInRepository(
                Company::class,
                [
                    'id' => 653,
                    'name' => 'my awesome company',
                    'created_at' => new DateTime('now'),
                ]
            );

        $this->service->associate(
            $this->em->find(Company::class, $companyId),
            $this->em->find(User::class, $userId)
        );

        $this
            ->tester
            ->dontSeeInRepository(User::class, ['company' => null]);

        $this
            ->tester
            ->seeInRepository(
                User::class,
                ['company' => ['name' => 'my awesome company']]
            );
    }
}
