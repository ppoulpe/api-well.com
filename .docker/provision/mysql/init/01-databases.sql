# create databases
CREATE DATABASE IF NOT EXISTS `wellcome`;
CREATE DATABASE IF NOT EXISTS `wellcome_test`;

# create root user and grant rights
GRANT ALL PRIVILEGES ON *.* TO 'wc_user'@'%';